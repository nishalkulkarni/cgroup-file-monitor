#include "r-app-monitor.h"

#include <gio/gio.h>
#include <glib-unix.h>
#include <glib.h>
#include <stdlib.h>

static gboolean
quit_mainloop (GMainLoop *loop)
{
  g_debug ("Exiting mainloop");

  if (g_main_loop_is_running (loop))
    g_main_loop_quit (loop);

  return G_SOURCE_CONTINUE;
}

static void
active_window_changed_cb (RAppMonitor *self)
{
  g_debug ("Active Window Changed.");
}

gint
main (gint argc, gchar *argv[])
{
  g_autoptr (GMainLoop) loop = NULL;
  g_autoptr (RAppMonitor) monitor = NULL;

  loop = g_main_loop_new (NULL, FALSE);

  g_unix_signal_add (SIGTERM, G_SOURCE_FUNC (quit_mainloop), loop);
  g_unix_signal_add (SIGINT, G_SOURCE_FUNC (quit_mainloop), loop);

  monitor = r_app_monitor_new ();

  r_app_monitor_start (monitor);

  g_signal_connect_object (monitor, "changed",
                           G_CALLBACK (active_window_changed_cb), monitor,
                           G_CONNECT_SWAPPED);

  g_main_loop_run (loop);

  return EXIT_SUCCESS;
}
