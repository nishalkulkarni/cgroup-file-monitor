#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <unistd.h>

#include <glib-object.h>
#include <glib.h>
#include <glib/gprintf.h>

#define XATTR_SIZE 10000

/**
 * Sets the timestamp attribute for a given path
 *
 * @return gboolean whether the attribute was set successfully
 */
gboolean
set_inactive_timestamp_xattr (gchar *path, gboolean inactive)
{
  gint64 current_time;
  gchar *value;

  if (inactive)
    {
      current_time = g_get_real_time ();
      value = g_strdup_printf ("%ld", current_time);
    }
  else
    {
      value = g_strdup_printf ("%ld", -1L);
    }

  if (setxattr (path, "user.xdg.inactive-since", value, strlen (value), 0))
    {
      return FALSE;
    }

  return TRUE;
}

/**
 * Gets the timestamp attribute for a given path
 *
 * @return gint64 0 on failure, -1 if currently active, else last active
 * timestamp
 */
gint64
get_inactive_timestamp_xattr (gchar *path)
{
  gchar ret_val[XATTR_SIZE];
  gchar *value;
  gint64 inactive_since;
  ssize_t value_len;

  value_len = getxattr (path, "user.xdg.inactive-since", ret_val, XATTR_SIZE);

  if (value_len == -1)
    {
      g_debug ("Couldn't get value of attribute.");
      return 0;
    }
  else
    {
      value = g_strdup_printf ("%.*s", (int)value_len, ret_val);
      inactive_since = g_ascii_strtoll (value, NULL, 0);
      return inactive_since;
    }
}

/**
 * Prints all of the attributes along with their values
 */
void
list_all_xattr (gchar *path)
{
  gchar list[XATTR_SIZE], ret_val[XATTR_SIZE];
  ssize_t list_len, value_len;

  list_len = listxattr (path, list, XATTR_SIZE);
  g_debug ("List len %zu", list_len);
  if (list_len < 0)
    {
      g_debug ("No extended attributes found.");
      return;
    }

  for (int ns = 0; ns < list_len; ns += strlen (&list[ns]) + 1)
    {
      g_debug ("NAME=%s; ", &list[ns]);

      value_len = getxattr (path, &list[ns], ret_val, XATTR_SIZE);
      if (value_len == -1)
        {
          g_debug ("Couldn't get value of attribute.");
        }
      else
        {
          g_debug ("VALUE=%.*s", (int)value_len, ret_val);
        }
    }
}

int
main (int argc, char *argv[])
{
  char *path;
  gint64 ts;

  if (argc != 2)
    {
      g_debug ("1 argument required");
      exit (1);
    }

  path = argv[1];
  g_debug ("The path is: %s", path);

  if (!set_inactive_timestamp_xattr (path, TRUE))
    {
      g_debug ("Failed to set attribute");
      exit (1);
    }

  ts = get_inactive_timestamp_xattr (path);
  if (ts)
    {
      g_debug ("VALUE=%ld", ts);
    }

  return 0;
}
