#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <unistd.h>

#include <glib-object.h>
#include <glib.h>
#include <glib/gprintf.h>

#include "r-app-info.h"

struct _RAppInfo
{
  GObject parent_instance;
};

G_DEFINE_TYPE (RAppInfo, r_app_info, G_TYPE_OBJECT);

RAppInfo *
r_app_info_new (void)
{
  return g_object_new (R_TYPE_APP_INFO, NULL);
}

static void
r_app_info_finalize (GObject *object)
{
  RAppInfo *self = (RAppInfo *)object;

  G_OBJECT_CLASS (r_app_info_parent_class)->finalize (object);
}

static void
r_app_info_class_init (RAppInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = r_app_info_finalize;


}

static void
r_app_info_init (RAppInfo *self)
{

}
