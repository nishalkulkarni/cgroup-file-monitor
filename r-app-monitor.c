#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <unistd.h>

#include <glib-object.h>
#include <glib.h>
#include <glib/gprintf.h>

#include "r-app-monitor.h"

#define BUF_LEN (10 * (sizeof (struct inotify_event) + NAME_MAX + 1))

struct _RAppMonitor
{
  GObject parent_instance;

  uid_t uid;
  gchar *path;
  gint inotify_fd;

  GIOChannel *channel;
};

G_DEFINE_TYPE (RAppMonitor, r_app_monitor, G_TYPE_OBJECT);

RAppMonitor *
r_app_monitor_new (void)
{
  return g_object_new (R_TYPE_APP_MONITOR, NULL);
}

static void
r_app_monitor_finalize (GObject *object)
{
  RAppMonitor *self = (RAppMonitor *)object;

  G_OBJECT_CLASS (r_app_monitor_parent_class)->finalize (object);
}

/**
 * Add cgroup directory to inotify watch
 *
 * @return gboolean FALSE if couldn't add directory, else TRUE
 */
gboolean
inotify_add_cgroup_dir (gint inotify_fd, gchar *path)
{
  gint wd;
  wd = inotify_add_watch (inotify_fd, path, IN_ATTRIB | IN_CREATE | IN_DELETE);
  if (wd == -1)
    return FALSE;

  g_debug ("Watching %s using wd %d\n", path, wd);
  return TRUE;
}

/*
 * Display information from inotify_event structure
 */
static void
print_inotify_event (struct inotify_event *i)
{
  g_printf ("wd =%2d; ", i->wd);
  if (i->cookie > 0)
    g_printf ("cookie =%4d; ", i->cookie);

  g_printf ("mask = ");
  if (i->mask & IN_ATTRIB)
    g_printf ("IN_ATTRIB ");
  if (i->mask & IN_CREATE)
    g_printf ("IN_CREATE ");
  if (i->mask & IN_DELETE)
    g_printf ("IN_DELETE ");
  if (i->mask & IN_IGNORED)
    g_printf ("IN_IGNORED ");
  if (i->mask & IN_ISDIR)
    g_printf ("IN_ISDIR ");
  g_printf ("\n");

  if (i->len > 0)
    g_printf ("name = %s\n", i->name);
}

static gboolean
inotify_data (GIOChannel *channel, GIOCondition cond, gpointer user_data)
{
  struct inotify_event *event;
  RAppMonitor *self = R_APP_MONITOR (user_data);
  GIOStatus status;
  gchar buffer[BUF_LEN];
  gsize bytes_read;
  gchar *p;

  (void)cond;
  (void)user_data;

  status = g_io_channel_read_chars (channel, buffer, sizeof (buffer) - 1,
                                    &bytes_read, NULL);

  if (status == G_IO_STATUS_NORMAL)
    {
      for (p = buffer; p < buffer + bytes_read;)
        {
          event = (struct inotify_event *)p;
          print_inotify_event (event);

          g_signal_emit_by_name (self, "changed");

          p += sizeof (struct inotify_event) + event->len;
        }
    }

  return TRUE;
}

void
r_app_monitor_start (RAppMonitor *self)
{
  if (!inotify_add_cgroup_dir (self->inotify_fd, self->path))
    g_error ("inotify_add_watch for app.slice directory failed");

  g_io_add_watch (self->channel, G_IO_IN | G_IO_HUP | G_IO_NVAL | G_IO_ERR,
                  inotify_data, self);
}

static void
r_app_monitor_class_init (RAppMonitorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = r_app_monitor_finalize;

  g_signal_new ("changed", R_TYPE_APP_MONITOR, G_SIGNAL_RUN_LAST, 0, NULL,
                NULL, NULL, G_TYPE_NONE, 0);
}

static void
r_app_monitor_init (RAppMonitor *self)
{
  self->uid = getuid ();
  self->path = g_strdup_printf ("/sys/fs/cgroup/user.slice/user-%1$i.slice/"
                                "user@%1$i.service/app.slice",
                                self->uid);

  self->inotify_fd = inotify_init ();
  if (self->inotify_fd < 0)
    g_error ("inotify_init failed");

  self->channel = g_io_channel_unix_new (self->inotify_fd);
  if (!self->channel)
    {
      close (self->inotify_fd);
      g_error ("g_io_channel_unix_new failed");
    }

  g_io_channel_set_close_on_unref (self->channel, TRUE);
  g_io_channel_set_encoding (self->channel, NULL, NULL);
  g_io_channel_set_buffered (self->channel, FALSE);
}
