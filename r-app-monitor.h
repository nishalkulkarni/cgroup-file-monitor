#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define R_TYPE_APP_MONITOR (r_app_monitor_get_type ())

G_DECLARE_FINAL_TYPE (RAppMonitor, r_app_monitor, R, APP_MONITOR, GObject)

RAppMonitor *r_app_monitor_new (void);

void r_app_monitor_start (RAppMonitor *self);

G_END_DECLS