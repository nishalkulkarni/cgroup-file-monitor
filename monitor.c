#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <unistd.h>

#include <glib-object.h>
#include <glib.h>
#include <glib/gprintf.h>

#define BUF_LEN (10 * (sizeof (struct inotify_event) + NAME_MAX + 1))

/**
 * Add cgroup directory to inotify watch
 *
 * @return gboolean FALSE if couldn't add directory, else TRUE
 */
gboolean
inotify_add_cgroup_dir (gint inotify_fd, gchar *path)
{
  gint wd;
  wd = inotify_add_watch (inotify_fd, path, IN_ATTRIB | IN_CREATE | IN_DELETE);
  if (wd == -1)
    {
      g_debug ("Error: inotify_add_watch");
      return FALSE;
    }

  g_debug ("Watching %s using wd %d\n", path, wd);
  return TRUE;
}

/*
 * Display information from inotify_event structure
 */
static void
print_inotify_event (struct inotify_event *i)
{
  g_printf ("wd =%2d; ", i->wd);
  if (i->cookie > 0)
    g_printf ("cookie =%4d; ", i->cookie);

  g_printf ("mask = ");
  if (i->mask & IN_ACCESS)
    g_printf ("IN_ACCESS ");
  if (i->mask & IN_ATTRIB)
    g_printf ("IN_ATTRIB ");
  if (i->mask & IN_CLOSE_NOWRITE)
    g_printf ("IN_CLOSE_NOWRITE ");
  if (i->mask & IN_CLOSE_WRITE)
    g_printf ("IN_CLOSE_WRITE ");
  if (i->mask & IN_CREATE)
    g_printf ("IN_CREATE ");
  if (i->mask & IN_DELETE)
    g_printf ("IN_DELETE ");
  if (i->mask & IN_DELETE_SELF)
    g_printf ("IN_DELETE_SELF ");
  if (i->mask & IN_IGNORED)
    g_printf ("IN_IGNORED ");
  if (i->mask & IN_ISDIR)
    g_printf ("IN_ISDIR ");
  if (i->mask & IN_MODIFY)
    g_printf ("IN_MODIFY ");
  if (i->mask & IN_MOVE_SELF)
    g_printf ("IN_MOVE_SELF ");
  if (i->mask & IN_MOVED_FROM)
    g_printf ("IN_MOVED_FROM ");
  if (i->mask & IN_MOVED_TO)
    g_printf ("IN_MOVED_TO ");
  if (i->mask & IN_OPEN)
    g_printf ("IN_OPEN ");
  if (i->mask & IN_Q_OVERFLOW)
    g_printf ("IN_Q_OVERFLOW ");
  if (i->mask & IN_UNMOUNT)
    g_printf ("IN_UNMOUNT ");
  g_printf ("\n");

  if (i->len > 0)
    g_printf ("name = %s\n", i->name);
}

static gboolean
inotify_data (GIOChannel *channel, GIOCondition cond, gpointer user_data)
{
  struct inotify_event *event;
  GIOStatus status;
  gchar buffer[BUF_LEN];
  gsize bytes_read;
  gchar *p;

  (void)cond;
  (void)user_data;

  status = g_io_channel_read_chars (channel, buffer, sizeof (buffer) - 1,
                                    &bytes_read, NULL);

  if (status == G_IO_STATUS_NORMAL)
    {
      for (p = buffer; p < buffer + bytes_read;)
        {
          event = (struct inotify_event *)p;
          print_inotify_event (event);

          p += sizeof (struct inotify_event) + event->len;
        }
    }

  return TRUE;
}

int
main (int argc, char *argv[])
{
  g_autoptr (GMainLoop) loop = NULL;
  GIOChannel *channel;
  int inotify_fd;
  char *path;
  uid_t uid;

  if (argc != 2)
    {
      uid = getuid ();
      path = g_strdup_printf ("/sys/fs/cgroup/user.slice/user-%1$i.slice/"
                              "user@%1$i.service/app.slice",
                              uid);
    }
  else
    {
      path = argv[1];
    }

  g_debug ("The path is: %s", path);

  loop = g_main_loop_new (NULL, FALSE);

  inotify_fd = inotify_init ();
  if (inotify_fd == -1)
    {
      g_debug ("Error: inotify_init failed");
      exit (1);
    }

  if (!inotify_add_cgroup_dir (inotify_fd, path))
    {
      exit (1);
    }

  channel = g_io_channel_unix_new (inotify_fd);
  if (!channel)
    {
      close (inotify_fd);
      exit (1);
    }
  g_io_channel_set_close_on_unref (channel, TRUE);
  g_io_channel_set_encoding (channel, NULL, NULL);
  g_io_channel_set_buffered (channel, FALSE);

  g_io_add_watch (channel, G_IO_IN | G_IO_HUP | G_IO_NVAL | G_IO_ERR,
                  inotify_data, NULL);

  g_main_loop_run (loop);

  return 0;
}
