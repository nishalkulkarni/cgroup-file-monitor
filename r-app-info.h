#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define R_TYPE_APP_INFO (r_app_info_get_type ())

G_DECLARE_FINAL_TYPE (RAppInfo, r_app_info, R, APP_INFO, GObject)

RAppInfo *r_app_info_new (void);

G_END_DECLS